const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const mongoose = require('mongoose');
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(methodOverride('_method'));

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

//Mongo URI
const mongoURI = 'mongodb://localhost/testdb';

//Create connection
const conn = mongoose.createConnection(mongoURI, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

//init var for gridfs stream
let gfs;

conn.once('open', () => {
	//init stream
	gfs = Grid(conn.db, mongoose.mongo);
	gfs.collection('file-uploads');
});

//create storage engine
let storage = new GridFsStorage({
	url: mongoURI,
	file: (req, file) => {
		return new Promise((resolve, reject) => {
			let filename = file.originalname;
			let fileInfo = {
				filename: filename,
				bucketName: 'file-uploads'
			};
			resolve(fileInfo);
		});
	}
});
const upload = multer({ storage });

// @route GET /
// @desc loads main page
app.get('/', (req, res) => {
	res.render('index');
});

//@route POST /upload
//@desc uploads file to db
app.post('/upload', upload.single('file'), (req, res) => {
	/*res.json({
		file: req.file
	});*/
	res.redirect('/');
});

//@route GET /files
//@desc  display all files in json
app.get('/files', (req, res) => {
	gfs.files.find().toArray((err, files) => {
		// check if files exist
		if (!files || files.length === 0) {
			return res.status(404).json({
				err: 'no files found'
			});
		}
		res.render('songs', {matched_songs: files});
	});
});

//@route GET /files/:filename
//@desc  display one file in json
app.get('/files/:filename', (req, res) => {
	gfs.files.findOne( { filename: req.params.filename }, (err, file) => {
		if (!file || file.length === 0) {
			return res.status(404).json({ err: 'no file found' });
		}
		let readstream = gfs.createReadStream(file.filename);
		readstream.pipe(res);
	})
});

app.get('/play', (req, res) => {
	let readstream = gfs.createReadStream(req.query['song']);
	readstream.pipe(res);
});

app.listen(port, (req, res) => {
	console.log(`listening on port ${port}`);
});
